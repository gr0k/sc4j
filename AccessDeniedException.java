package sc4j;

/**
 * Kernel32 Error_access_denied errno
 * @author surreal
 */
public class AccessDeniedException extends Exception {
    
    public AccessDeniedException() {
    }
    
    public AccessDeniedException(String message) {
        super(message);
    }
    
}
