/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sc4j;

/**
 *
 * @author surreal
 */
public class ServiceDisabledException extends Exception {
    
    public ServiceDisabledException() {
    }
    
    public ServiceDisabledException(String message) {
        super(message);
    }
    
}
