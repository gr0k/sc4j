/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sc4j;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author surreal
 */
abstract public class JarUtility {

    /**
     * Copies the file to the System32 directory.
     * Useful for running or loading resources from a jar
     * @param filePath
     * @return 
     */
    public static void exportLibrary(String filePath) throws IOException {
        String fullName = FilenameUtils.getName(filePath);
        String ext = FilenameUtils.getExtension(filePath);
        String dest = "\\windows\\System32\\" + fullName;
        
        if(new File(dest).exists())
            return;

        InputStream in = ClassLoader.getSystemResourceAsStream(filePath);
        try (FileOutputStream out = new FileOutputStream(dest)) {
            IOUtils.copy(in, out);
        }
        
    }
}
