/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sc4j;

/**
 *
 * @author surreal
 */
public class PathNotFoundException extends Exception {
    
    public PathNotFoundException() {
    }
    
    public PathNotFoundException(String message) {
        super(message);
    }
    
}
