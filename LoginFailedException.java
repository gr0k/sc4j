/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sc4j;

/**
 *
 * @author surreal
 */
public class LoginFailedException extends Exception {
    
    public LoginFailedException() {
    }
    
    public LoginFailedException(String message) {
        super(message);
    }
    
}
