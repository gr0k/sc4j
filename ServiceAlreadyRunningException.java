/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sc4j;

/**
 *
 * @author surreal
 */
public class ServiceAlreadyRunningException extends Exception {
    
    public ServiceAlreadyRunningException() {
    }
    
    public ServiceAlreadyRunningException(String message) {
        super(message);
    }
    
}
