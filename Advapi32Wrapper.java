/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sc4j;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.Advapi32;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.DWORDByReference;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.platform.win32.Winsvc;
import com.sun.jna.platform.win32.Winsvc.SC_HANDLE;
import com.sun.jna.win32.W32APIOptions;

/**
 * Wrapper for JNA Advapi32 library for loading and controlling Windows services
 * @author surreal
 */
public class Advapi32Wrapper {
    
    /* JNA Library missing CreateService entry, so we add it */
    public interface Advapi32 extends com.sun.jna.platform.win32.Advapi32 {
        SC_HANDLE CreateService(SC_HANDLE scManager,
                String serviceName,
                String displayName,
                int access,
                int serviceType,
                int startType,
                int errControl,
                String binaryPath,
                String loadOrderGroup,
                DWORDByReference lpdwTagId,
                String dependencies,
                String serviceStartName,
                String password);
    }
    
    Advapi32 api;
    
    /**
     * Instantiates Advapi32 library, loading the appropriate dll.
     * Will throw exception if loading fails.
     * @throws LibraryException 
     */
    public Advapi32Wrapper() throws LibraryException {
        api = (Advapi32)Native.loadLibrary("Advapi32.dll", Advapi32.class, W32APIOptions.UNICODE_OPTIONS);
        if(api == null)
            throw new LibraryException("Failed to load Advapi32 library");
    }
    
    /**
     * Return a handle to the Service Control manager.
     * This is required to then open, create, or control services.
     * See MSDN OpenSCManager
     * @return 
     */
    public SC_HANDLE OpenSCManager() {
        return api.OpenSCManager(null, null, Winsvc.SC_MANAGER_ALL_ACCESS);
    }
    
    /**
     * Opens a handle to a service for later control.
     * alias is the registered service alias.
     * See MSDN OpenService
     * @param manager
     * @param alias
     * @return 
     */
    private SC_HANDLE OpenService(SC_HANDLE manager, String alias) {
        return api.OpenService(manager, alias, Winsvc.SERVICE_ALL_ACCESS);
    }
    
    /**
     * Convenience wrapper for CreateService and OpenService.
     * If the service does not exist, it will create it and return the handle.
     * If it does exist, it will open it and return the handle.
     * Loads from \windows\System32 directory
     * @param manager
     * @param binary
     * @param alias
     * @return 
     */
    public SC_HANDLE LoadService(SC_HANDLE manager, String binary, String alias) {
        SC_HANDLE handle =  api.CreateService(manager, alias, alias, Winsvc.SERVICE_ALL_ACCESS, WinNT.SERVICE_KERNEL_DRIVER, 2, 1, "\\windows\\System32\\" + binary, null, null, null, null, null);
        if((handle == null) && (Native.getLastError() == Kernel32.ERROR_SERVICE_EXISTS))
            handle = OpenService(manager, alias);
        
        return handle;
    }
    
    /**
     * Starts the specified windows service.
     * See MSDN StartService
     * @param service
     * @throws AccessDeniedException
     * @throws InvalidHandleException
     * @throws LoginFailedException
     * @throws PathNotFoundException
     * @throws ServiceAlreadyRunningException
     * @throws ServiceDisabledException
     * @throws OtherException 
     */
    public void StartService(SC_HANDLE service) throws AccessDeniedException,
            InvalidHandleException, LoginFailedException, PathNotFoundException,
            ServiceAlreadyRunningException, ServiceDisabledException, OtherException {
        if(api.StartService(service, 0, null))
            return;
        
        switch(Native.getLastError()) {
            case Kernel32.ERROR_ACCESS_DENIED:
                throw new AccessDeniedException();
            case Kernel32.ERROR_INVALID_HANDLE:
                throw new InvalidHandleException();
            case Kernel32.ERROR_PATH_NOT_FOUND:
                throw new PathNotFoundException();
            case Kernel32.ERROR_SERVICE_ALREADY_RUNNING:
                throw new ServiceAlreadyRunningException();
            case Kernel32.ERROR_SERVICE_DISABLED:
                throw new ServiceDisabledException();
            case Kernel32.ERROR_SERVICE_LOGON_FAILED:
                throw new LoginFailedException();
            default:
                throw new OtherException("Failed with errno: " 
                        + Native.getLastError());
        }
    }
    
}
